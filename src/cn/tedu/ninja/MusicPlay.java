package cn.tedu.ninja;


import java.io.FileInputStream;
import java.io.IOException;

import sun.audio.AudioData;
import sun.audio.AudioDataStream;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

public class MusicPlay {
	private AudioData audiodata;
	private AudioDataStream audiostream;
	public MusicPlay(String filename)  {
	    FileInputStream fis;
		try {
			fis = new FileInputStream(filename);
			@SuppressWarnings("resource")
			AudioStream audioStream = new AudioStream(fis);
		    audiodata = audioStream.getData();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	  }
		//播放
	  public void play() {
	    audiostream = new AudioDataStream(audiodata);
	    AudioPlayer.player.start(audiostream);
	  }
	  //循环播放
	  public void loop(){
		  ContinuousAudioDataStream cads = new ContinuousAudioDataStream(audiodata);
			//循环播放开始哦
		  AudioPlayer.player.start(cads);
	  }
	  //停止播放
	  public void stop() {
		    audiostream = new AudioDataStream(audiodata);
		    AudioPlayer.player.stop(audiostream);
	  }
}
