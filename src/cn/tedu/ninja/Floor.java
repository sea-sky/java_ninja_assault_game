package cn.tedu.ninja;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

	/**
	 * 地板类
	 * 每一块地板有7-13个地板块构成
	 *
	 */
public class Floor {
	protected FloorBlock[]  floorblocks;
	private Random ran;
	public int x;
	public int y;
	public int width;
	public int height;
	public Floor(int t){
		x=0;
		y=300+66;
		floorblocks=new  FloorBlock[t];
		width=t*FloorBlock.width;
		height=FloorBlock.height;
//		super(width,height,x,y);
		for(int i=0;i<t;i++){

			floorblocks[i]=new FloorBlock(x+i*FloorBlock.width, y);
		}
	}
	
	
	public Floor(){
		ran=new Random();
		int length=ran.nextInt(7)+22;
		floorblocks=new FloorBlock[length];
		x=World.WIDTH-100;
		y=(int)((Math.random()*0.3+0.5)*World.HEIGHT);
		width=length*FloorBlock.width;
		height=FloorBlock.height;
		for(int i=0;i<length;i++){
//			floorblocks[i].setX(x+i*FloorBlock.width);
			floorblocks[i]=new FloorBlock(x+i*FloorBlock.width, y);
		}
	}
	public Floor(int x,int t){
		this.x=x;
		y=300+66;
		floorblocks=new  FloorBlock[t];
		width=t*FloorBlock.width;
		height=FloorBlock.height;
//		super(width,height,x,y);
		for(int i=0;i<t;i++){

			floorblocks[i]=new FloorBlock(x+i*FloorBlock.width, y);
		}
	}
	public void paintFloor(Graphics g) {
		
		x=floorblocks[0].getX();
		y=floorblocks[0].getY();
		for(int i=0;i<floorblocks.length;i++){
			
			g.drawImage(floorblocks[i].getImage(), x+FloorBlock.width*i, y, null);
		}
	}
	
	public void move(){
		for(int i=0;i<floorblocks.length;i++){
			floorblocks[i].move();
		}
	}
	
	/** 检测x坐标在不在地板上方*/
	public boolean isin(AllObject other) {
		int x1 = this.x-other.width-5;  //x1:地板的x-英雄的宽
		int x2 = this.x+this.width-26;   //x2:地板的x+地板的宽
//		int y1 = other.y; //y1:地板的y-英雄的高
//		int y2 = other.y;  //y2:地板的y
		int x = other.x;              //x:英雄的x
//		int y = other.y;              //y:英雄的y
		return x>=x1 && x<=x2 ;
			   
//			   y>=y1 && y<=y2;
	}
	//
//	public boolean isonFloor(AllObject other){
//		int y=other.y;
//		int y1=this.y+other.height+10;
//		int y2=this.y+other.height-10;
//		return y>y2&&y<y1;
//	}
	/** 越过英雄的检查 */
	public boolean outOfHero(AllObject other) {
		return this.x+this.width<=other.x+30; //x<=0，即为越界了
	}
	/** 越界检查 */
	public boolean outOfBounds() {
		return this.x+this.width+30<=0; //x<=0，即为越界了
	}
}
