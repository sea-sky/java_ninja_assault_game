package cn.tedu.ninja;

import java.awt.image.BufferedImage;

/**
 * 要画那条悬空的小黑线。
 * 英雄一次跳跃跨越不过的无地板区域上空出现天花板，
 * 然后才能悬空的设定。
 * @author Administrator
 *
 */
public class Hanging extends AllObject{
	private static BufferedImage img;
	static{
		img=loadImage("/image/hang.png");
	}
	private int speed;
	public Hanging(int x, int y) {
		super(15,38);
		this.x = x;
		this.y = y;
		speed=3;
	}
	/*
	 * 小黑线移动的方法
	 */
	public void step(){
		
	}
	//呈现图片
	public  BufferedImage getImage(){
		return img;
	}
}
