package cn.tedu.ninja;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * 一块块地板
 * @author Administrator
 *
 */
public class FloorBlock extends AllObject {
	public static int width=26;
	public static int height=26;
	private int x;
	private int y;
	public int speed;
	private static BufferedImage img;
	static{
		img=loadImage("/image/mu.png");
	}
	public  BufferedImage getImage(){
		return img;
	}
	public FloorBlock(int x,int y) {
		super(29, 32);
		this.x = x;
		this.y = y;
		speed=4;
	}
	public void move() {
		x-=speed;
		
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
}
