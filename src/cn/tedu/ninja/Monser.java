package cn.tedu.ninja;

import java.awt.image.BufferedImage;

/**
 * 怪物类
 * @author Administrator
 *
 */
public class Monser extends AllObject{
	private static BufferedImage[] image1;
	private static BufferedImage[] image2;
	static{
		image1=new BufferedImage[9];
		image2=new BufferedImage[1];
		for(int i=0;i<image1.length;i++){
			image1[i]=loadImage("/image/enemy"+i+".png");
		}
		for(int i=0;i<image2.length;i++){
			image2[i]=loadImage("/image/enemy0.png");
		}
	}
	private int speed;
	
	public Monser(int x, int y) {
		super(44,84);
		this.x = x;
		this.y = y;
		speed=4;
		
	}
	/*
	 * 怪物在地板上移动的方法
	 */
	public void step(){
		x-=speed;
	}
	int index = 1; //图片下标
	//呈现图片
	public  BufferedImage getImage(){
		if(isLife()||isUnhurt()) { //若活着的，返回images[0]
			return image1[index++/10%image1.length];
		}else if(isDead()) { //若死了的
				state = REMOVE; //将当前状态修改为REMOVE
		}
		return null; //删除的，返回null
	}
	//setSpeed
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
}
