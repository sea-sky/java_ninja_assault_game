package cn.tedu.ninja;

import java.awt.image.BufferedImage;

/**
 * 飞镖类
 * @author Administrator
 *
 */
public class Darts extends AllObject{
	private static BufferedImage image;
	static{
			image=loadImage("/image/dart.png");
	}
	private int speed;
	public Darts(int x, int y) {
		super(14,14,x,y);
		speed=2;
	}
	/*
	 * 飞镖移动的方法
	 */
	public void step(){
		x+=speed;
	}
	//呈现图片
	public  BufferedImage getImage(){
		if(isLife()) { //若活着的，返回images[0]
			return image;
		}else if(isDead()) { //若死了的
				state = REMOVE; //将当前状态修改为REMOVE
		}
		return null; //删除的，返回null
	}
	/** 越界检查 */
	public boolean outOfBounds() {
		return this.x>World.WIDTH+1; //x<=0，即为越界了
	}
}
