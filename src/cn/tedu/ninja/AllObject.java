package cn.tedu.ninja;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.imageio.ImageIO;

/**
 * 超类抽象类
 * 存放所有类的公有属性
 * @author Administrator
 *
 */
public abstract class AllObject {
	public static final int LIFE = 0;   //活着的
	public static final int DEAD = 1;   //死了的
	public static final int REMOVE = 2; //删除的
	public static final int UNHURT = 3; //无伤害能力的
	protected int state = LIFE; //当前状态(默认为活着的)
	
	protected int width;  //宽
	protected int height; //高
	protected int x;      //x坐标
	protected int y;      //y坐标
	/*
	 * 给宽高值给定的类的构造方法
	 */
	public AllObject(int width, int height) {
		super();
		this.width = width;
		this.height = height;
		this.x=World.WIDTH-this.width;
		Random  rand=new Random();
		this.y=rand.nextInt(World.HEIGHT/3-this.height);
	}
	//都给定的构造方法
	public AllObject(int width, int height, int x, int y) {
		super();
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
	}
	/** 读取图片 */
	public static BufferedImage loadImage(String fileName) {
		try {
			BufferedImage img = ImageIO.read(AllObject.class.getResource(fileName)); //同包下读取图片
			return img;
		}catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	/** 获取图片 */
	public abstract BufferedImage getImage();
	/** 画对象 g:画笔 */
	public void paintObject(Graphics g) {
		g.drawImage(this.getImage(),this.x,this.y,null); //不要求掌握
	}
	
	/** 越界检查 */
	public boolean outOfBounds() {
		return this.x+this.width<=0; //x<=0，即为越界了
	}
	/*
	 * 判斷是否活著
	 */
	public boolean isLife(){
		return state==LIFE;
	}
	public boolean isDead(){
		return state==DEAD;
	}
	public boolean isRemove(){
		return state==REMOVE;
	}
	public boolean isUnhurt(){
		return state==UNHURT;
	}
	/** 检测碰撞*/
	public boolean hit(AllObject other) {
		int x1 = this.x-other.width;  //x1:怪物的x-英雄的宽
		int x2 = this.x+this.width;   //x2:怪物的x+怪物的宽
		int y1 = this.y-other.height; //y1:怪物的y-英雄的高
		int y2 = this.y+this.height;  //y2:怪物的y+怪物的高
		int x = other.x;              //x:飞镖的x
		int y = other.y;              //y:飞镖的y
		
		return x>=x1 && x<=x2 
			   && 
			   y>=y1 && y<=y2; //x在x1与x2之间，并且，
			                   //y在y1与y2之间，即为撞上了
	}
	
	/** 敌人去死 */
	public void goDead() {
		state = DEAD; //将对象状态修改为DEAD(死了的)
	}
	/*无伤害能力*/
	public void goUnhurt() {
		state = UNHURT; //将对象状态修改为DEAD(死了的)
	}
}
