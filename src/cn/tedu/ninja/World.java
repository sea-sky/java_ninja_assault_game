package cn.tedu.ninja;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * 世界类，添加游戏进程展示  空格跳跃，J飞镖
 * @author Administrator
 *
 */
public class World extends JPanel implements KeyListener,MouseListener{
	public static final int WIDTH=860;
	public static final int HEIGHT=500;
	public static boolean Jump;//跳判断空格键是否按下
	public static int J=2;//限制有二连跳
	public static boolean onGround;//英雄脚下是否有地板
	public static int maxHeight=260;//获取地板的最低高度
	public static boolean shootJ;//射击键技能J
	public static boolean SJ;//限制按一次J发一次飞镖
	public static boolean isFirstFloor;//是否游戏初始化需要第一块地板
	public static boolean isHurt;//是否不跳到上面的地板，这样应该受伤。
	
	public static int score=0;
	public static int bsIndex=1;
	public static final int START = 0;     //启动状态
	public static final int RUNNING = 1;   //运行状态
	public static final int PAUSE = 2;     //暂停状态
	public static final int GAME_OVER = 3; //游戏结束状态
	public static final int BIGIN = 4; //初始化状态
	private int state = BIGIN; //当前状态(默认为启动状态)
	private static BufferedImage start; //启动图
	private static BufferedImage pause; //暂停图
	private static BufferedImage gameover; //游戏结束图
	private static BufferedImage begin; //初始图
	private static BufferedImage energy; //能量
	private static BufferedImage blood; //血量
	private static BufferedImage[] hurtblood; //受伤 
	private static BufferedImage[] book1; //版面图片
	private static BufferedImage[] book2; //版面图片 
	private static MusicPlay heroshoot;	private static MusicPlay eat;
	private static MusicPlay herohurt;	private static MusicPlay dead;
	private static MusicPlay boss_die;	private static MusicPlay crow_die;
	private static MusicPlay moner_die; private static BGplayer bg;
	static { //初始化静态资源
		start = AllObject.loadImage("/image/start.png");
		pause = AllObject.loadImage("/image/pause.png");
		gameover = AllObject.loadImage("/image/gameover.png");
		begin=AllObject.loadImage("/image/be.png");
		energy = AllObject.loadImage("/image/n.png");
		blood= AllObject.loadImage("/image/blood.png");
		hurtblood= new BufferedImage[7];
		book2=new BufferedImage[5];book1=new BufferedImage[5];
		for(int i=0;i<hurtblood.length;i++){
			hurtblood[i]=AllObject.loadImage("/image/hurtblood"+i+".png");
		}
		book1[0]=AllObject.loadImage("/image/gamebox.png");
		book1[1]=AllObject.loadImage("/image/测试.png");
		book1[2]=AllObject.loadImage("/image/开始.png");
		book1[3]=AllObject.loadImage("/image/退出.png");
		book1[4]=AllObject.loadImage("/image/tryagin.png");
		book2[0]=AllObject.loadImage("/image/pd.png");
		book2[1]=AllObject.loadImage("/image/back.png");
		heroshoot=new MusicPlay("shoot.wav");eat=new MusicPlay("eat.wav");
		herohurt=new MusicPlay("hurt.wav"); dead=new MusicPlay("dead.wav");
		boss_die=new MusicPlay("boss_die.wav"); moner_die=new MusicPlay("enemy_die.wav");
		crow_die=new MusicPlay("crow_hurt.wav");
		bg=new BGplayer();
	}
	private Hero hero=new Hero();
	private Boss boss;
	private List<BossShoot> shoot=new ArrayList<BossShoot>();
	private Background background=new Background();
	private static List<Floor> floors=new ArrayList<Floor>();
	private List<Monser> monsers=new ArrayList<Monser>();
	private List<Threaten> threaten=new ArrayList<Threaten>();
	private List<Darts> darts=new ArrayList<Darts>();
	private List<Crow> crows=new ArrayList<Crow>();
	private List<Award> awards=new ArrayList<Award>();
//	//增加第一块地板
//	static{
//		addone();
//	}
	//第一块地板
	private  void addone() {
		floors.add(new Floor(28));
	}
	/*
	 * 重写paint方法
	 * 每个类的paint方法最终汇总在这里
	 */
	Font font=new Font("Consolas", Font.TRUETYPE_FONT,70);
	int hurtindex=0;
	public void paint(Graphics g){
		background.paintObject(g);
		hero.paintObject(g);
		synchronized (crows) {
			if(crows.size()>0){
				for(Crow c:crows){
					c.paintObject(g);;
				}
			}
		}
		if(boss!=null){
			boss.paintObject(g);
		}
		synchronized (floors) {
			for(Floor f:floors){
				f.paintFloor(g);
			}
		}
		synchronized (threaten) {
			if(threaten.size()!=0){
				for(Threaten t:threaten){
					t.paintObject(g);;
				}
			}
		}
		synchronized (monsers) {
			if(monsers.size()!=0){
				for(Monser m:monsers){
					m.paintObject(g);;
				}
			}
		}
		synchronized (darts) {
			if(darts.size()!=0){
				for(Darts d:darts){
					d.paintObject(g);
				}
			}
		}
		synchronized (awards) {
			if(awards.size()>0){
				for(Award a:awards){
					a.paintObject(g);
				}
			}
		}
		synchronized (shoot) {
			if(shoot.size()>0){
				for(BossShoot s:shoot){
					s.paintObject(g);;
				}
			}
		}
		g.setColor(Color.white);
		g.drawString("血量：",10,20); //画命
		g.drawString("能量：",10,55); //画充能
		g.drawString("分数："+score,10,75); //画分
		switch (hero.getLife()) {
		case 1:
			g.drawImage(blood,45,0,null);
			break;
		case 2:
			g.drawImage(blood,45,0,null);
			g.drawImage(blood,75,0,null);
			break;
		case 3:
			g.drawImage(blood,45,0,null);
			g.drawImage(blood,75,0,null);
			g.drawImage(blood,105,0,null);
			break;
		case 4:
			g.drawImage(blood,45,0,null);
			g.drawImage(blood,75,0,null);
			g.drawImage(blood,105,0,null);
			g.drawImage(blood,135,0,null);
			break;	
		case 5:
			g.drawImage(blood,45,0,null);
			g.drawImage(blood,75,0,null);
			g.drawImage(blood,105,0,null);
			g.drawImage(blood,135,0,null);
			g.drawImage(blood,165,0,null);
			break;	
		}
		switch (hero.getEnergy()) {
		case 1:
			g.drawImage(energy,45,30,null);
			break;
		case 2:
			g.drawImage(energy,45,30,null);
			g.drawImage(energy,75,30,null);
			break;	
		}
		if(hero.isHurt){
			if(hurtindex<7){
			g.drawImage(hurtblood[hurtindex++%hurtblood.length],hero.x,hero.y+22,null);
			}else{
				herohurt.play();
			hero.isHurt=false;hurtindex=0;}
		}
		switch(state) { //根据当前状态画不同的图
		case START: //启动状态时画启动图
			g.drawImage(start,0,0,null);g.drawImage(book2[0],570,355,null);
			g.drawImage(book2[1],30,370,null);
			break;
		case PAUSE: //暂停状态时画暂停图
			g.drawImage(pause,0,0,null);
			break;
		case GAME_OVER: //游戏结束状态时画游戏结束图
			bg.stopMusic();
			if(!hero.isDead()){
				dead.play();
			}
			hero.goDead();
			g.drawImage(gameover,0,0,null);g.drawImage(book1[4],550,310,null);
			g.drawImage(book2[1],30,320,null);
			g.setFont(font);
			g.setColor(Color.BLACK);
			g.drawString(""+score,535,226);
			break;
		case BIGIN: //初始化
			bg.stopMusic();
			g.drawImage(begin,0,0,null);g.drawImage(book1[2],400,110,null);
			g.drawImage(book1[1],400,190,null);g.drawImage(book1[3],400,270,null);
			break;
		}
	}
	Random rand=new Random();
	private int enterindex=0,enter,ifenter;
	int cIndex=1;
	/**进场！！！
	 * 让地板和地板上的怪物或者铁刺进场
	 */
	public void floorEnter(){
		enter=rand.nextInt(100);ifenter=rand.nextInt(28);
		if(enterindex++%((13*26/2)+26*2)==0){
				score++;bsIndex++;
				Floor floornext=new Floor();
//				floorcurrent=floornext;
				if(ifenter<12){
					synchronized (threaten) {
						if(ifenter<2&&boss==null){
//							int the=rand.nextInt(floornext.width-110);
							threaten.add(new Threaten(floornext.x+rand.nextInt(floornext.width/2-55), floornext.y-50));
							threaten.add(new Threaten(floornext.x+floornext.width/2+rand.nextInt(floornext.width/2-55), floornext.y-50));
							
						}else{
							threaten.add(new Threaten(floornext.x+rand.nextInt(floornext.width-55), floornext.y-50));
						}
					}
				}else if(ifenter<26){
					synchronized (monsers) {
						if(ifenter<14){
							monsers.add(new Monser(floornext.x+rand.nextInt(floornext.width-43), floornext.y-82));
						}else{
							monsers.add(new Monser(floornext.x+rand.nextInt(floornext.width/2-43), floornext.y-82));
							monsers.add(new Monser(floornext.x+floornext.width/2+rand.nextInt(floornext.width/2-43), floornext.y-82));
						}
					}
				}else{
					synchronized (awards) {
						awards.add(new Award(floornext.x+rand.nextInt(floornext.width-60), floornext.y-55));
					}
				}
				
				synchronized (floors) {
					floors.add(floornext);
				}
		}
		if(cIndex++%(180+enter)==0&&boss==null){
			synchronized (crows) {
				crows.add(new Crow());
			}
		}
		//boss进场
		if(bsIndex%20==0&&boss==null){
			boss=new Boss(90);
		}
	}
	/**
	 * 无论初始界面和开始游戏都可以运行的方法
	 */
	public void stateAction(){
		if(Jump&&hero.hState!=3){
			hero.jump();
			hero.hState=1;
			if(hero.i>38){
				Jump=false;
				hero.hState=2;
			}
		} 
		if(((!onGround)||hero.hState==2)&&hero.hState!=3){
			hero.down();
//			hero.hState=2;
		}else if(hero.hState!=3){hero.hState=0;}
		synchronized (floors) {
			for(Floor f:floors){
				f.move();
			}
		}
		//英雄火力
//		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		if(shootJ&&SJ==false){
			synchronized (darts) {
				darts.add(hero.dart());
			}
			SJ=true;
		}
		synchronized (darts) {
			if(darts.size()!=0){
				for(Darts d:darts){
					d.step();
				}
			}
		}
		List<Darts> dartsLive=new ArrayList<Darts>();
		synchronized (darts) {
			for(Darts d:darts){
				if(!d.isRemove()&&!d.outOfBounds()) { //
					dartsLive.add(d);
				}
			}
			darts=dartsLive;
		}
		List<Floor> floorsLive=new ArrayList<Floor>();
		synchronized (floors) {
			for(Floor f:floors){
				if(!f.outOfBounds()) { //
					floorsLive.add(f);
				}
			}
			floors=floorsLive;
		}
	}
	//所有物体运动方法
	public void stepAction(){
//		background.step();
		synchronized (threaten) {
			if(threaten.size()!=0){
				for(Threaten t:threaten){
					t.step();
				}
			}
		}
		synchronized (monsers) {
			if(monsers.size()!=0){
				for(Monser m:monsers){
					m.step();
				}
			}
		}
//		System.out.println("=============="+monsers.size());
		synchronized (crows) {
			if(crows.size()>0){
				for(Crow c:crows){
					c.step();
				}
			}
		}
		synchronized (awards) {
			if(awards.size()>0){
				for(Award a:awards){
					a.step();
				}
			}
		}
		synchronized (shoot) {
			for(BossShoot s:shoot){
				s.step();
			}
		}
	}
	/**
	 * 碰撞算法；
	 */
	public void hitAction(){
		synchronized (darts) {
			if(darts.size()>0){
				for(Darts d:darts){
					synchronized (monsers) {
						if(monsers.size()>0){
							for(Monser m:monsers){
								if(d.isLife()&&m.x<(WIDTH-m.width) && m.isLife() && d.hit(m)) { //撞上了
									d.goDead(); //飞镖去死
									moner_die.play();
									m.goDead(); //敌人去死
									score+=1;
								}
							}
						}
					}	
					synchronized (crows) {
						if(crows.size()>0){
							for(Crow c:crows){
								if(d.isLife()&&c.x<(WIDTH-c.width) && c.isLife() && d.hit(c)) { //撞上了
									d.goDead(); //飞镖去死
									crow_die.play();
									c.goDead(); //敌人去死
									score+=2;
								}
							}
						}
					}
					/*
					 * 打boss；
					 */
					if(boss!=null){
					if(d.isLife() && boss.isLife() && boss.hit(d)) { //撞上了
						d.goDead(); //飞镖去死
						boss.subtractLife(); //boss减命
					}
					}
				}
			}
		}
		/*
		 * 英雄与怪物碰撞
		 */
		synchronized (monsers) {
			if(monsers.size()>0){
				for(Monser m:monsers){
					if(!hero.isRemove() && m.isLife() && m.hit(hero)) { //撞上了
						if(hero.hState!=3){
							hero.dLife();
							hero.clearEnergy();
							m.goUnhurt();
						}else{
							moner_die.play();
							m.goDead();
						}
					}
				}
			}
		}	
		synchronized (crows) {
			if(crows.size()>0){
				for(Crow c:crows){
					if(!hero.isRemove() && c.isLife() && c.hit(hero)) { //撞上了
						if(hero.hState!=3){
							hero.dLife();
							hero.clearEnergy();
							c.goUnhurt();
						}else{
							crow_die.play();
							c.goDead();
						}
					}
				}
			}
		}
		synchronized (threaten) {
			if(threaten.size()>0){
				for(Threaten t:threaten){
					if(hero.isLife() && t.isLife() && t.hit(hero)) { //撞上了
						if(hero.hState!=3){
							hero.dLife();
							hero.clearEnergy();
							t.goUnhurt();
						}
					}
				}
			}
		}
		synchronized (shoot) {
			if(shoot.size()>0){
				for(BossShoot b:shoot){
					if(!hero.isRemove() &&b.isLife() && b.hit(hero)) { //撞上了
						if(hero.hState!=3){
							hero.dLife();
//							hero.clearEnergy();
						}
						b.goDead();
					}
				}
			}
		}	
		//奖励碰撞
		synchronized (awards) {
			if(awards.size()>0){
				for(Award a:awards){
					if(hero.isLife() && a.isLife() && a.hit(hero)) { //撞上了
						eat.play();
						a.goDead();
						switch (a.getType()) {
						case 0:
							hero.addLife();
							break;
						case 1:
							hero.addEnergy();
							break;
						}
					}
				}
			}
		}
	}
	/*
	 * 创建新的集合将没有死亡活越界的拿回来。相当于删除越界和死亡
	 */
	public void deleteAction(){
		List<Crow> crowsLive=new ArrayList<Crow>();
		synchronized (crows) {
			for(Crow c:crows){
				if(!c.isRemove()&&!c.outOfBounds()) { //撞上了
					crowsLive.add(c);
				}
			}
			crows=crowsLive;
		}
		List<Monser> monsersLive=new ArrayList<Monser>();
		synchronized (monsers) {
			for(Monser m:monsers){
				if(!m.isRemove()&&!m.outOfBounds()) { //撞上了
					monsersLive.add(m);
				}
			}
			monsers=monsersLive;
		}
		List<Threaten> TLive=new ArrayList<Threaten>();
		synchronized (threaten) {
			for(Threaten t:threaten){
				if(!t.outOfBounds()) { //
					TLive.add(t);
				}
			}
			threaten=TLive;
		}
		List<Award> awardsLive=new ArrayList<Award>();
		synchronized (awards) {
			for(Award a:awards){
				if(!a.isRemove()&&!a.outOfBounds()) { //撞上了
					awardsLive.add(a);
				}
			}
			awards=awardsLive;
		}
		List<BossShoot> shootLive=new ArrayList<BossShoot>();
		synchronized (shoot) {
			for(BossShoot b:shoot){
				if(!b.isRemove()&&!b.outOfBounds()) {
					shootLive.add(b);
				}
			}
			shoot=shootLive;
		}
	}
	
	//英雄发出飞镖
	int bShoot=1;
	public void shootAction(){
		//boss火力
		if(boss!=null){
			if(bShoot++%200==0){
				int st=rand.nextInt(9);
				if(st<7) {
					synchronized (shoot) {
						shoot.add(boss.bshoot());
					}
				}else{
					synchronized (crows) {
						crows.add(boss.crow());
					}
				}
			}
	}
		}
	/**
	 * 游戏的整体演示类
	 */
	public void Action(){
		Timer timer=new Timer(); 
		timer.schedule(new TimerTask(){
			public void run() {
				if(state==RUNNING){
					if(!isFirstFloor){
						addone();
						isFirstFloor=true;
						bg.loopMusic();
					}
//					if(!isRun){
//						run.loop();
//						isRun=true;
//					}
//					if(hero.hState==1&&isRun){
//						run.stop();
//					}else if(hero.hState==0&&!isRun){
//						isRun=false;
//					}
					
					floorEnter();
					stateAction();
					setOnGround();
					stepAction();
					shootAction();
					hitAction();
					if(onGround&&hero.y>World.maxHeight-hero.height+4){
						if(hero.y>World.maxHeight-hero.height+39&&hero.hState!=3){
							state=GAME_OVER;
						}else{
							hero.y=World.maxHeight-hero.height+4;}
					}
					checkAction();
				}else if(state==START){
					if(!isFirstFloor){
						floors.add(new Floor(67));
						isFirstFloor=true;
						bg.loopMusic();
					}
					setOnGround();
					stateStart();
					stateAction();
				}

				repaint();
			}

		}, 10, 10);
	}
	
	public static void main(String[] args) {
		JFrame jf=new JFrame();
		World world=new World();
		jf.add(world);
		jf.addKeyListener(world);
		jf.addMouseListener(world);
		
		jf.setSize(WIDTH, HEIGHT);
		jf.setLocationRelativeTo(null);
		//jf.setUndecorated(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);
		world.Action();
//		if(world.state==START||world.state==RUNNING){
//			World.bg.loop();
//		}
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int x=arg0.getX();int y=arg0.getY();
		switch(state) { //根据状态的不同做不同的处理
		case START:        //启动状态时
			clear();
			if(x>10&&x<250&&y>350&&y<450){
				state=BIGIN;
			}else{
				clear();
				state=RUNNING; //修改为运行状态
				isFirstFloor=false;
			}
			break;
		case GAME_OVER:  //游戏结束状态时
			clear();
			if(x>10&&x<250&&y>310&&y<410){
				state=BIGIN;
			}else{
				state=RUNNING; //修改为运行状态
			}
			break;
		case BIGIN:  //初始化状态时
			clear();
			if(x>390&&x<710&&y>105&&y<223){
				state=RUNNING;
			}else if(x>390&&x<710&&y>=227&&y<315){
				state=START;
			}else if(x>390&&x<710&&y>=315&&y<405){
				System.exit(0);
			}
			break;	
		}
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		if(state==PAUSE) { //暂停状态时
			state=RUNNING; //修改为运行状态
		}
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		if(state==RUNNING) { //运行状态时
			state=PAUSE;     //修改为暂停状态
		}
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		
		
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		switch (arg0.getKeyCode()) {
		case KeyEvent.VK_SPACE:
			if(J>0&&!Jump&&hero.hState!=3){
				Jump=true;
				hero.hState=1;
				hero.i=0;
				hero.d=84;
				J-=1;
			}
			
		break;
		case KeyEvent.VK_J:
			if(hero.hState!=3&&state!=GAME_OVER&&state!=PAUSE){
			shootJ=true;
			heroshoot.play();
			}
		break;
		case KeyEvent.VK_ENTER:
			switch (state) {
			case START:
				clear();
				state=RUNNING; //修改为运行状态
				isFirstFloor=false;
				break;
			case GAME_OVER:
				clear();
				state=RUNNING;
				break;
			case BIGIN:
				clear();
				state=START;
				break;
			}
		break;
		case KeyEvent.VK_ESCAPE:
			switch(state) { //根据当前状态画不同的图
			case START: //启动状态时画启动图
				state=BIGIN;
				break;
			case GAME_OVER: //游戏结束
				state=BIGIN;
				break;
			case RUNNING: //游戏开始
				state=BIGIN;
				break;
			case BIGIN: //初始化
				System.exit(0);
				break;
			}
		break;
		} 
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		switch (arg0.getKeyCode()) {
		case KeyEvent.VK_SPACE:
		break;
		case KeyEvent.VK_J:
			shootJ=false;SJ=false;
		break;
		}
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
	
		
	}
	//判断英雄脚下是否有地板
	public void setOnGround(){
		synchronized (floors) {
			for(Floor f:floors){
				if (f.isin(hero)) {
					onGround = true;
					Floor theone=f;
					maxHeight=theone.y;
					if(theone.outOfHero(hero)){
						onGround = false;
						isHurt=false;
						if(hero.hState!=3){
						hero.hState=2;}
					}
				}
			}
		}
	}
	/** 检测游戏结束 */
	int heroUndead=1;
	public void checkAction() { //每10毫秒走一次
		if(hero.getLife()<=0||(hero.y>(maxHeight+6)&&(J<1))||(hero.y>(maxHeight+2*hero.height))) { //游戏结束了
			state = GAME_OVER; //将当前状态修改为游戏结束状态
		}
		if(boss!=null){
			if(hero.isLife()&&boss.getLife()<=0){
				boss.goDead();
				if(boss!=null&&boss.isDead()){
					boss_die.play();
				}
				boss=null;
				bsIndex=1;
			}
		}
		if(hero.getEnergy()>=3){
			hero.hState=3;
			J=2;Jump=false;
			hero.clearEnergy();
		}
		if(hero.hState==3){
			if(boss!=null){
				boss.goDead();
				boss=null;
				bsIndex=1;
			}
			if(heroUndead++>662){
				score+=50;
				hero.hState=2;
				heroUndead=1;
			}
			
		}
	}
	/*清除数据的方法*/
	public void clear(){
		score = 0;   //清理现场
		isFirstFloor=false;
		enterindex=0;bsIndex=1;startindex=0;
		J=2;Jump=false;heroUndead=1;SJ=false;//这些控制数不清零会影响游戏效果
//		onGround=true;
		background = new Background();
//		bg=new BGplayer();
		floors=new ArrayList<Floor>();
		monsers=new ArrayList<Monser>();
		threaten=new ArrayList<Threaten>();
		darts=new ArrayList<Darts>();
		crows=new ArrayList<Crow>();
		awards=new ArrayList<Award>();
		boss=null;
		shoot=new ArrayList<BossShoot>();
		hero = new Hero();
	}
	/**
	 *	公共方法，游戏处于初始界面的试玩
	 */
	private int startindex=1;
	public void stateStart(){
		if(onGround&&hero.y>World.maxHeight-hero.height+4){
			hero.y=World.maxHeight-hero.height+4;}
		synchronized(floors){
			if(startindex++%((13*26/2)+26*2)==0){
				floors.add(new Floor(WIDTH, 34));
			}
		}
	}	
}
