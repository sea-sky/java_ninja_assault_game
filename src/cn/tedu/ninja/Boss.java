package cn.tedu.ninja;

import java.awt.image.BufferedImage;
import java.util.Random;
/**
 * Boss类
 * @author Administrator
 *
 */
public class Boss extends AllObject{
	private static BufferedImage[] images;
	private static BufferedImage[] imageH;
	static{
		images=new BufferedImage[4];
		imageH=new BufferedImage[4];
		for(int i=0;i<images.length;i++){
			images[i]=loadImage("/image/boss"+i+".png");
		}
		for(int i=0;i<imageH.length;i++){
			imageH[i]=loadImage("/image/bossH"+i+".png");
		}
	}
	private int life;
	private int speed;
	public Boss(int y) {
		super(170,180,World.WIDTH-170,y);
//		this.x=World.WIDTH-this.width;
//		this.y = y;
		speed=3;
		life=60;
	}
	/*
	 * Boss跟着英雄移动
	 */
	public void step(int y){
		
	}
	//呈现图片
	int index;
	public  BufferedImage getImage(){
		if(life>14){
			return images[index++/30%images.length];
		}else if(life>0){
			return imageH[index++/30%images.length];
		}else if(isDead()){
			state=REMOVE;
			index=0;
		}
		return null;
	}
	/** 获取boss的生命值 */
	public int getLife() {
		return life; //返回命数
	}
	/** boss的生命值减少 */
	int bIndex=0;
	//int startLife=life;
	public void subtractLife() {
		if(life>0){
		life-=1;
		}
	}
	//技能火力
	public BossShoot bshoot(){
		int xStep = 55-45;
		int yStep = this.height-40-9;
		BossShoot bs= new BossShoot(this.x+xStep,this.y+yStep);
		return bs;
	}
	//发射乌鸦
	public Crow crow(){
		int xStep = 55-42;
		int yStep = this.height-40-40;
		Crow crow= new Crow(this.x+xStep,this.y+yStep);
		return crow;
	}
	//重写 发生碰撞的方法
	public boolean hit(AllObject other){
		int x1=this.x-other.width+10;   //敌人的x坐标-子弹/英雄的宽度
		int x2=this.x+this.width;		//敌人的x坐标+敌人的宽度
		int y1=this.y-other.height+2;	//敌人的y坐标-子弹/英雄的高度
		int y2=this.y+this.height-20;		//敌人的y坐标+敌人的高度
		int x=other.x;
		int y=other.y;
		return x>=x1&&x<=x2&&y>=y1&&y<=y2;// 算法  在范围内即碰撞
	}
}
