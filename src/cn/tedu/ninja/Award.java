package cn.tedu.ninja;

import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 奖励道具类
 * @author Administrator
 *
 */
public class Award extends AllObject{
//	private int width;
//	private int height;
//	private int x;
//	private int y;
	private static BufferedImage[] images;
	static{
		images=new BufferedImage[2];
		images[0] = loadImage("/image/award1.png");
		images[1] = loadImage("/image/award2.png");
	}
	private int speed;
	//奖励的类型，不同类型作用不同
	private int type;
	public Award(int x,int y) {
		super(60,52);
		this.x = x;
		this.y = y;
		speed=4;
		Random rand=new Random();
		type=rand.nextInt(2);
	}
	/*
	 * 道具跟着地板移动
	 */
	public void step(){
		x-=speed;
	}
	//获知奖励类型来画图片和增加英雄属性
	public int getType() {
		return type;
	}
	/** 重写getImage()获取图片 */
	public BufferedImage getImage() { //每10毫秒走一次
		if(isLife()&&type<1){
			return images[0];
		}else if(isLife()&&type==1){
			width=46;
			return images[1];
		}else if(isDead()){
			state=REMOVE;
		}
		return null;
		/*
		 *                   index=0
		 * 10M 返回images[0] index=1
		 * 20M 返回images[1] index=2
		 * 30M 返回images[0] index=3
		 * 40M 返回images[1] index=4
		 * 50M 返回images[0] index=5
		 */
	}
}
