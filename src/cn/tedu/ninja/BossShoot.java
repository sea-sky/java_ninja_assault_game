package cn.tedu.ninja;

import java.awt.image.BufferedImage;

/**
 * Boss发射的威胁物
 * @author Administrator
 *
 */
public class BossShoot extends AllObject{
	private static BufferedImage image;
	static{
		image=loadImage("/image/bs.png");
	}
	private int speed;
	public BossShoot(int x, int y) {
		super(30,15,x,y);
		speed=2;
	}
	/*
	 * Boss发射的威胁移动
	 */
	public void step(){
		x-=speed;
	}

	// 呈现图片
	public BufferedImage getImage() {
		if(isLife()) { //若活着的，返回images[0]
			return image;
		}else if(isDead()) { //若死了的
				state = REMOVE; //将当前状态修改为REMOVE
		}
		return null; //删除的，返回null
	}
}
