package cn.tedu.ninja;

import java.awt.image.BufferedImage;


/**
 * 英雄类
 * @author Administrator
 *s
 */
public class Hero extends AllObject{
	//private static BufferedImage[] imgs;
//	private static  int JUMP_SPEED = 6;//跳跃速度
	public int i = 0;//跳跃速度
	public static final int RUN=0;
	public static final int JUMP=1;
	public static final int DOWN=2;
	public static final int UNDEAD=3;
	public int hState=RUN;
	public static boolean isRun;//是否奔跑
    public boolean isHurt=false;//是否受伤
	private static BufferedImage[] run;
	private static BufferedImage[] unHero;
	private static BufferedImage shootHero;
	private static BufferedImage jumpHero;
	private static BufferedImage downHero;
	private static BufferedImage heroi;
//	private boolean thread=false;
	static{
		run=new BufferedImage[10];
		unHero=new BufferedImage[5];
		shootHero=loadImage("/image/shoothero.png");
		jumpHero=loadImage("/image/jump.png");
		downHero=loadImage("/image/down.png");
		heroi=loadImage("/image/hero001.png");
		for(int i=0;i<run.length;i++){
			run[i]=loadImage("/image/run"+i+".png");
		}
		for(int i=0;i<unHero.length;i++){
			unHero[i]=loadImage("/image/unhero"+i+".png");
		}
	}
	private int life;
	private int speed;
	//吃到的能量
	private int energy;
	private int v0=260;
	private int g=-160;
	public Hero() {
		super(51,66,110,480-180);
		life=3;
		speed=2;
		energy=2;
		
	}
	/*
	 * 部分get和set方法
	 */
	private int index=0;public int unindex=0;
	public  BufferedImage getImage(){
		if(World.shootJ){
			return shootHero;
		}else if(World.Jump){
			return jumpHero;
		}
		else {
			switch (hState) {
			case DOWN:
				return downHero;	
			case RUN:
				return run[index++/10%run.length];
			case UNDEAD:
				return unHero[index++/5%unHero.length];	
			}
		return heroi;	
		}
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public int getLife() {
		return life;
	}
	public int getEnergy() {
		return energy;
	}
	public void setEnergy(int energy) {
		this.energy = energy;
	}
	
	
	/*
	 * 英雄移动的方法
	 * 好像只有跳跃和悬空了
	 */
	public void jump(){
		y-=(v0+v0+(1+2*i++)*g*0.02)*0.01;
//		onGround=false; 
	}
	int d=84;
	public  void down(){
		if(World.onGround&&y>=World.maxHeight-height+4){
			hState=RUN;
			d=84;
			World.J=2;
		}else{
			y-=(v0+v0+(1+2*d++)*g*0.02)*0.01;
		}
	}

	//是否在地板上方
	public boolean isOnFloor(){
		return false;
	} 
	/*
	 * 增命和增energy
	 */
	public void addLife(){
		if(life<5){
			life++;
		}
	}
	public void addEnergy(){
		energy++;
	}
	/*
	 * 减命和清能量瓶
	 */
	public void dLife(){
		life--;
		isHurt=true;
	}
	public void clearEnergy(){
		energy=0;
	}
	//技能火力
	public Darts dart(){
		int xStep = this.width;
		int yStep = this.height/3;
		Darts i=new Darts(this.x+xStep,this.y+yStep);
		return i;
	}
}
